<?php

use Civi\Api4\FinancialType;
use Civi\Api4\OptionGroup;
use Civi\Api4\OptionValue;
use CRM_Civisplit_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Civisplit_Upgrader extends CRM_Civisplit_Upgrader_Base {

  // By convention, functions that look like "function upgrade_NNNN()" are
  // upgrade tasks. They are executed in order (like Drupal's hook_update_N).

  /**
   * Example: Work with entities usually not available during the install step.
   *
   * This method can be used for any post-install tasks. For example, if a step
   * of your installation depends on accessing an entity that is itself
   * created during the installation (e.g., a setting or a managed entity), do
   * so here to avoid order of operation problems.
   */
  public function postInstall() {
    $this->createFinancialType();
    $this->createOptionGroups();
  }

  public function upgrade_1002() {
    $this->createOptionGroups();
    return TRUE;
  }

  public function upgrade_1003() {
    $this->updateDatabaseTables();
    return TRUE;
  }

  private function createFinancialType() {
    // Create financial type and set as default
    if (FinancialType::get(FALSE)->addWhere('name', '=', 'CiviSplit')->execute()->rowCount === 0) {
      $civiSplitFinancialType = FinancialType::create(FALSE)
        ->addValue('name', 'CiviSplit')
        ->addValue('is_reserved', TRUE)
        ->addValue('is_active', TRUE)
        ->execute()
        ->first();
      \Civi::settings()->set('civisplit_financial_type', $civiSplitFinancialType['id']);
    }
  }

  private function createOptionGroups() {
    if (empty(OptionGroup::get(FALSE)
      ->addWhere('name', '=', 'civisplit_agreement_status')
      ->execute()
      ->first())) {
      OptionGroup::create(FALSE)
        ->setValues([
          'name' => 'civisplit_agreement_status',
          'title' => 'Civisplit Agreement Status',
          'is_reserved' => TRUE,
          'is_active' => TRUE,
          'data_type' => 'Integer',
          'is_locked' => TRUE,
        ])
        ->execute();
    }

    if (empty(OptionValue::get(FALSE)
      ->addWhere('option_group_id:name', '=', 'civisplit_agreement_status')
      ->addWhere('name', '=', 'draft')
      ->execute()
      ->first())) {
      OptionValue::create(FALSE)
        ->setValues([
          'option_group_id.name' => 'civisplit_agreement_status',
          'label' => 'Draft',
          'value' => 0,
          'name' => 'draft',
        ])
        ->execute();
    }

    if (empty(OptionValue::get(FALSE)
      ->addWhere('option_group_id:name', '=', 'civisplit_agreement_status')
      ->addWhere('name', '=', 'agreed')
      ->execute()
      ->first())) {
      OptionValue::create(FALSE)
        ->setValues([
          'option_group_id.name' => 'civisplit_agreement_status',
          'label' => 'Agreed',
          'value' => 1,
          'name' => 'agreed',
        ])
        ->execute();
    }

    if (empty(OptionValue::get(FALSE)
      ->addWhere('option_group_id:name', '=', 'civisplit_agreement_status')
      ->addWhere('name', '=', 'completed')
      ->execute()
      ->first())) {
      OptionValue::create(FALSE)
        ->setValues([
          'option_group_id.name' => 'civisplit_agreement_status',
          'label' => 'Completed',
          'value' => 2,
          'name' => 'completed',
        ])
        ->execute();
    }
  }

  private function updateDatabaseTables() {
    if (!CRM_Core_BAO_SchemaHandler::checkIfFieldExists('civicrm_civisplit_agreement', 'is_test')) {
      CRM_Upgrade_Incremental_Base::addColumn($this->ctx, 'civicrm_civisplit_agreement', 'is_test', "tinyint DEFAULT 0 COMMENT 'Is this a test agreement?'");
    }
  }

}
