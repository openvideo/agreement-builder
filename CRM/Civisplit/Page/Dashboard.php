<?php
use CRM_Civisplit_ExtensionUtil as E;

class CRM_Civisplit_Page_Dashboard extends CRM_Core_Page {

  public function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(E::ts('Dashboard'));

    Civi::resources()->addBundle('bootstrap3');

    $agreement = \Civi\Api4\CivisplitAgreement::get(FALSE)
      ->addSelect('*', 'created_id.display_name')
      ->addWhere('is_test', 'IN', [TRUE, FALSE])
      ->execute();
    foreach ($agreement as &$agreementDetail) {
      // To access in Smarty!
      $agreementDetail['created_id_display_name'] = $agreementDetail['created_id.display_name'];
    }
    $this->assign('CiviSplitAgreement', $agreement);
    $this->assign('CiviSplitAgreementString', print_r($agreement,TRUE));


    //

    parent::run();
  }


}
