<?php
use CRM_Civisplit_ExtensionUtil as E;
use Brick\Money\Money;

class CRM_Civisplit_Page_Report extends CRM_Core_Page {

  public function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(E::ts('Report'));

    Civi::resources()->addBundle('bootstrap3');

    $agreementHash = CRM_Utils_Request::retrieveValue('hash', 'String', NULL, FALSE, 'GET');
    $agreementID = CRM_Utils_Request::retrieveValue('id', 'Int', NULL, FALSE, 'GET');
    $reportAPI = \Civi\Api4\CivisplitAgreement::getReport(FALSE)->setMoneyFormat('locale');
    if ($agreementHash) {
      $reportAPI->setHash($agreementHash);
    }
    elseif ($agreementID) {
      $reportAPI->setId($agreementID);
    }
    $report = $reportAPI->execute();

    $this->assign('latestLog', end($report['logs']));
    $this->assign('report', $report);
    // For debugging; this is output in a <pre style="display:none;" > element.
    $this->assign('reportPretty', json_encode($report, JSON_PRETTY_PRINT));
    parent::run();
  }

}
