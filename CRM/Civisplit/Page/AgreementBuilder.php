<?php
use CRM_Civisplit_ExtensionUtil as E;

class CRM_Civisplit_Page_AgreementBuilder extends CRM_Core_Page {

  public function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(E::ts('Agreement Builder'));

    Civi::resources()->addBundle('bootstrap3');

    parent::run();
  }

}
