<?php
use CRM_Civisplit_ExtensionUtil as E;

class CRM_Civisplit_Page_AgreementViewer extends CRM_Core_Page {

  public function run() {
    // Example: Set the page-title dynamically; alternatively, declare a static title in xml/Menu/*.xml
    CRM_Utils_System::setTitle(E::ts('Agreement Viewer'));

    Civi::resources()->addBundle('bootstrap3');

    $agreementHash = CRM_Utils_Request::retrieveValue('hash', 'String', NULL, TRUE);
    $agreement = \Civi\Api4\CivisplitAgreement::get(FALSE)
      ->addWhere('hash', '=', $agreementHash)
      ->execute()
      ->first();
    if (CRM_Utils_Request::retrieveValue('raw', 'Boolean', FALSE)) {
      print "<html><head><title>Agreement {$agreementHash}</title></head><body><h1>Agreement Name: {$agreement['name']}</h1>";
      print print_r($agreement,TRUE);
      print "</body></html>";
      CRM_Utils_System::civiExit();
    }
    else {
      /** @var string (previously this would have been an array */
      $agreementRsl = $agreement['agreement'];
      /** @var array */
      $agreementStructure = \Civi\Civisplit\Yaml::parse($agreementRsl);

     
      $this->assign('CiviSplitAgreement', $agreementStructure);
      $this->assign('CiviSplitAgreementFull', $agreement);
      $this->assign('CiviSplitAgreementHash', $agreementHash);
     }

    parent::run();
  }

}
