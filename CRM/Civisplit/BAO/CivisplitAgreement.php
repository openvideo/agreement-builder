<?php

use Civi\Api4\OptionValue;
use CRM_Civisplit_ExtensionUtil as E;

class CRM_Civisplit_BAO_CivisplitAgreement extends CRM_Civisplit_DAO_CivisplitAgreement {

  /**
   * Create a new CivisplitAgreement (also: update existing) based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_Civisplit_DAO_CivisplitAgreement|NULL
   */
  public static function create($params) {
    $className = 'CRM_Civisplit_DAO_CivisplitAgreement';
    $entityName = 'CivisplitAgreement';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);

    $instance = new $className();
    if (array_intersect(['id', 'name'], array_keys($params))) {
      // We have been given a unique identifier.
      if (!empty($params['id'])) {
        $instance->id = $params['id'];
      }
      else {
        $instance->name = $params['name'];
      }
      $instance->find(TRUE);
    }

    if (empty($instance->id)) {
      // New agreement
      // Check - do we have created_id?
      if (empty($params['created_id'])) {
        $params['created_id'] = CRM_Core_Session::getLoggedInContactID();
      }
    }
    else {
      // Update existing agreement
      // Check - cannot update an agreed agreement

      $statusNamesToStatusIDs = OptionValue::get(FALSE)
        ->addWhere('option_group_id:name', '=', 'civisplit_agreement_status')
        ->execute()
        ->indexBy('name')->column('value');

      $disallow = FALSE;
      if ((int) $instance->status_id === (int) $statusNamesToStatusIDs['agreed']) {
        // Trying to update an agreement in agreed state. We allow:
        // changing state to 'completed' only.
        // If data is sent for any of these, disallow it.
        $disallow |= (array_intersect_key($params, array_flip(['agreement', 'name', 'hash', 'created_date', 'created_id', 'is_test'])));
        // Disallow status changes except to completed.
        $disallow |= (array_key_exists('status_id', $params)
          ? $params['status_id'] != $statusNamesToStatusIDs['completed']
          : FALSE);
      }
      elseif ((int) $instance->status_id === (int) $statusNamesToStatusIDs['completed']) {
        // Do not allow any changes at all to completed agreements.
        $disallow = TRUE;
      }

      if ($disallow) {
        throw new CRM_Core_Exception('Cannot modify an agreement that has been agreed');
      }
    }

    $instance->copyValues($params);

    // If draft generate hash (each save)
    if ($instance->agreement) {
      $instance->hash = self::generateHash($instance->agreement);
    }
    else {
      $instance->hash = '';
    }

    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  }

  /**
   * Generate a hash for the RSL in the agreement.
   */
  public static function generateHash(string $rsl) {
    return hash('sha256', $rsl);
  }

}
