<?php
use CRM_Civisplit_ExtensionUtil as E;

class CRM_Civisplit_BAO_CivisplitAgreementPaymentProcessor extends CRM_Civisplit_DAO_CivisplitAgreementPaymentProcessor {

  /**
   * Create a new CivisplitAgreementPaymentProcessor based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_Civisplit_DAO_CivisplitAgreementPaymentProcessor|NULL
   *
  public static function create($params) {
    $className = 'CRM_Civisplit_DAO_CivisplitAgreementPaymentProcessor';
    $entityName = 'CivisplitAgreementPaymentProcessor';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);
    $instance = new $className();
    $instance->copyValues($params);
    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  } */

}
