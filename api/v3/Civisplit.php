<?php

use CRM_Civisplit_ExtensionUtil as E;

function _civicrm_api3_civisplit_fundsavailable_spec(&$spec) {
  // One of hash/id is required
  $spec['agreement_hash']['title'] = E::ts('Agreement Hash');
  $spec['agreement_hash']['type'] = CRM_Utils_Type::T_STRING;
  $spec['agreement_hash']['api.required'] = FALSE;
  $spec['agreement_id']['title'] = E::ts('Agreement ID');
  $spec['agreement_id']['type'] = CRM_Utils_Type::T_INT;
  $spec['agreement_id']['api.required'] = FALSE;
  $spec['amount_available']['title'] = E::ts('Amount Available');
  $spec['amount_available']['type'] = CRM_Utils_Type::T_FLOAT;
  $spec['amount_available']['api.required'] = TRUE;
}

function civicrm_api3_civisplit_fundsavailable($params) {
  civicrm_api3_verify_one_mandatory($params, NULL, ['agreement_hash', 'agreement_id']);
  try {
    if (isset($params['agreement_id']) && !isset($params['agreement_hash'])) {
      $params['agreement_hash'] = \Civi\Api4\CivisplitAgreement::get(FALSE)
        ->addWhere('id', '=', $params['agreement_id'])
        ->execute()
        ->first()['hash'];
    }
    \Civi\Civisplit\Event\FundsAvailable::trigger($params['agreement_hash'], $params['amount_available']);
    return civicrm_api3_create_success([]);
  }
  catch (Exception $e) {
    return civicrm_api3_create_error($e->getMessage());
  }
}

function _civicrm_api3_civisplit_approve_agreement_spec(&$spec) {
  $spec['agreement_id']['title'] = E::ts('Agreement ID');
  $spec['agreement_id']['type'] = CRM_Utils_Type::T_INT;
  $spec['agreement_id']['api.required'] = TRUE;
}

function civicrm_api3_civisplit_approve_agreement($params) {
  if (empty(\Civi\Api4\CivisplitAgreement::get(FALSE)
  ->addWhere('id', '=', $params['agreement_id'])
  ->execute()->first())) {
    throw new CiviCRM_API3_Exception("Agreement with ID: {$params['agreement_id']} not found");
  }
  try {
    \Civi\Api4\CivisplitAgreement::update(FALSE)
      ->addValue('status_id:name', 'agreed')
      ->addWhere('id', '=', $params['agreement_id'])
      ->execute();
  }
  catch (Exception $e) {
    throw new CiviCRM_API3_Exception($e->getMessage());
  }
  return civicrm_api3_create_success();
}
