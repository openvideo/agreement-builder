<?php
// This file declares a new entity type. For more details, see "hook_civicrm_entityTypes" at:
// https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
return [
  [
    'name' => 'CivisplitAgreementPaymentProcessor',
    'class' => 'CRM_Civisplit_DAO_CivisplitAgreementPaymentProcessor',
    'table' => 'civicrm_civisplit_agreement_payment_processor',
  ],
];
