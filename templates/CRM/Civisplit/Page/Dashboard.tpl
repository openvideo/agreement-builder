{crmStyle ext=civisplit file=css/cascade.css}

<div id="bootstrap-theme" >
    <h1><a class="btn btn-primary pull-right" href="{crmURL p='civicrm/agreement-builder/create'}"><i class="crm-i fa-plus"></i>&nbsp;{ts}Create new agreement{/ts}</a>
        {ts}Revenue Sharing Payout Plans{/ts}</h1>
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p>{ts}This extension is not a replacement for a lawyer or accountant! Currently in beta: use at your own risk.{/ts}</p>
    </div>
    <p>
    <table class="table table-hover table-striped">
        <thead>
          <tr><th>{ts}Agreement name{/ts}</th><th>{ts}Created by{/ts}</th><th>{ts}Status{/ts}</th><th>{ts}Created{/ts}</th><th>{ts}Generated{/ts}</th><th></th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$CiviSplitAgreement key=id item=agreementList}
            <tr>
                <td>
                    <strong>
                        <a target="" href="{crmURL p='civicrm/agreement-builder/create' q="reset=1&id=`$agreementList.id`"}">{$agreementList.name}</a>
                    </strong>
                </td>
              {capture assign=createdID}{$agreementList.created_id}{/capture}
                <td><a href='{crmURL p="civicrm/contact/view" q="reset=1&cid=$createdID"}'>{$agreementList.created_id_display_name}</a></td>
                <td>{if $agreementList.status_id eq '0'}{ts}<span class="badge badge-default">Draft</span>{/ts}
                    {elseif $agreementList.status_id eq '1'}{ts}<span class="badge badge-success">Generated</span>{/ts}
                    {elseif $agreementList.status_id eq '2'}{ts}<span class="badge badge-primary"></span>Completed</span>{/ts}
                  {/if}
                </td>
                <td>{$agreementList.created_date}</td>
                <td>{if $agreementList.status_id eq '0'}-
                    {else}{$agreementList.modified_date}{/if}
                </td>
                <td><span class="pull-right">
                    {if $agreementList.status_id eq '0'}
                        <a class="btn btn-primary" target="" href="{crmURL p='civicrm/agreement-builder/create' q="reset=1&id=`$agreementList.id`"}"><i class="crm-i fa-pencil-square-o"></i>&nbsp;{ts}Edit{/ts}
                        </a>
                    {/if}
                    {if $agreementList.status_id eq '1'}
                    <a class="btn btn-success" target="" href="report?reset=1&hash={$agreementList.hash}">
                      <i class="crm-i fa-pie-chart"></i>&nbsp;{ts}Report{/ts}
                    </a>
                    {/if}
                    <a class="btn btn-warning" href="view?reset=1&hash={$agreementList.hash}">
                    <i class="crm-i fa-th-list"></i>&nbsp;{ts}View{/ts}
                    </a>
                </span></td>
            </tr>
            {/foreach}
        </tbody>
    </table>
    </div>
