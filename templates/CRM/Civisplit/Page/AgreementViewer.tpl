{crmStyle ext=civisplit file=css/cascade.css}

<div id="bootstrap-theme" >

<div class="panel {if $CiviSplitAgreementFull.status_id eq '0'}panel-primary{else}panel-info{/if}" id="agreeement-viewer">
<div class="panel-heading">
  <h2 class="panel-title">{ts}{if $CiviSplitAgreementFull.status_id eq '0'}DRAFT - {/if}Revenue Sharing Payout Plan{/ts}</h2>
</div>
<div class="panel-body">
<p>{if $CiviSplitAgreementFull.status_id eq '0'}Last edited on {else}Generated and locked on {/if}{$CiviSplitAgreementFull.modified_date|date_format:"%A, %B %e, %Y"}</p>
<h1 class="margin0top">{$CiviSplitAgreement.name}</h1>
<h3>For {$CiviSplitAgreement.currency} receipts {if $CiviSplitAgreement.address != ''}at:{/if} <strong>{$CiviSplitAgreement.address}</strong></h3>
<p>{$CiviSplitAgreement.description}</p>
<div class="step-list">
  {foreach from=$CiviSplitAgreement.steps key=step item=stepDetail}
    <h4>{$stepDetail.description} – ({$stepDetail.type}) {if $stepDetail.cap != ''} – capped at {$stepDetail.cap}{/if}</h4>
    <ul>
        {foreach from=$stepDetail.payees key=payee item=payeeDetail}
          <li><strong>{$payeeDetail.paymentName}:</strong> {$payeeDetail.paymentAmount}{if $stepDetail.type eq 'percentage'}%{elseif $stepDetail.type eq 'ratio'} {ts}parts{/ts}{/if}{if $payeeDetail.paymentType}, {/if}{$payeeDetail.paymentType}{if $payeeDetail.paymentType eq 'civicrm'}, CiviCRM ID: {$payeeDetail.paymentAddress}{else}{$payeeDetail.paymentAddress}{/if}</li>
        {/foreach}
    </ul>
  {/foreach}
</div>
<details open>
  <summary><strong>Agreement RSL</strong> <em>(hash: {$CiviSplitAgreementHash})</em></strong></summary>
  <pre class="rsl">{$CiviSplitAgreementFull.agreement}<a href="https://github.com/openvideotech/rsl" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29.7 53.1" class="rsl-logotype"><defs><style>{literal}.a{fill:#aaa;}{/literal}</style></defs><path d="M16,24H13.38V5.62h5.9a10.87,10.87,0,0,1,2.52.28,6.15,6.15,0,0,1,2.09.87,4.3,4.3,0,0,1,1.42,1.57,5,5,0,0,1,.52,2.38A4.42,4.42,0,0,1,24.58,14a5.8,5.8,0,0,1-3.22,1.59L26.5,24H23.38l-4.67-8.19H16Zm0-10.35h3a8.93,8.93,0,0,0,1.69-.15A4.54,4.54,0,0,0,22,13a2.5,2.5,0,0,0,1.26-2.3,2.58,2.58,0,0,0-.34-1.39,2.5,2.5,0,0,0-.9-.9A3.91,3.91,0,0,0,20.68,8a8.11,8.11,0,0,0-1.59-.14H16Z" transform="translate(-1.59 -2.87)"/><path d="M23.11,33a4.13,4.13,0,0,0-1.48-1.24,4.55,4.55,0,0,0-2.11-.48,5.09,5.09,0,0,0-1.26.17,3.4,3.4,0,0,0-1.15.55,3,3,0,0,0-.85.93,2.61,2.61,0,0,0-.32,1.34,2.4,2.4,0,0,0,.3,1.26,2.52,2.52,0,0,0,.8.86,5.17,5.17,0,0,0,1.2.6l1.47.51c.62.19,1.25.4,1.89.65a6.59,6.59,0,0,1,1.75,1,4.82,4.82,0,0,1,1.27,1.49,4.7,4.7,0,0,1,.49,2.28,5.37,5.37,0,0,1-.53,2.48,5.13,5.13,0,0,1-1.41,1.74,6,6,0,0,1-2.05,1,8.25,8.25,0,0,1-2.38.34,8.66,8.66,0,0,1-3.36-.68,6.66,6.66,0,0,1-2.69-2l2-1.69a4.95,4.95,0,0,0,5.41,2,3.71,3.71,0,0,0,1.18-.58,3.25,3.25,0,0,0,.87-1,2.74,2.74,0,0,0,.34-1.42,2.61,2.61,0,0,0-.35-1.39,3.19,3.19,0,0,0-.95-1,6.18,6.18,0,0,0-1.39-.66l-1.68-.56a14,14,0,0,1-1.74-.64,6.06,6.06,0,0,1-1.54-1,4.5,4.5,0,0,1-1.09-1.44,5,5,0,0,1-.41-2.13A4.67,4.67,0,0,1,13.9,32a5.08,5.08,0,0,1,1.45-1.64,6.21,6.21,0,0,1,2-.95,8.79,8.79,0,0,1,2.27-.3,8,8,0,0,1,3,.58,5.94,5.94,0,0,1,2.26,1.53Z" transform="translate(-1.59 -2.87)"/><path class="a" d="M1.59,2.87h5v48.6H31.29V56H1.59Z" transform="translate(-1.59 -3.47)" style="fill: #aaa;"/></svg></a></pre>
</details> 
</div>
</div>
</div>
