{*   Add CSS *}
{crmStyle ext=civisplit file=css/cascade.css}

{*  Add JS: the weight is essential; cascade.js must be loaded first. (N.b. using 'defer' would be nice here, too, but does not seem supported by Civi.) *}
{crmScript ext=civisplit file=js/cascade.js weight=1}
{crmScript ext=civisplit file=js/cascade-civi.js weight=2}

<div id="bootstrap-theme" >
  <h1>CiviSplit - Agreement Builder</h1>
  <div id="cascade-ui-placeholder"></div>
</div>
