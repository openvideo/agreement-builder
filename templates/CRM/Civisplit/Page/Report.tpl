{crmStyle ext=civisplit file=css/cascade.css}

<div id="bootstrap-theme">
<h2>{$report.name} - report {if $report.status_id eq '0'}({ts}Draft{/ts})
    {elseif $report.status_id eq '2'}({ts}Completed{/ts})
    {/if}</h2><a href="view?reset=1&hash={$report.hash}" class="btn btn-primary btn-small pull-right">View Agreement</a>
<p><strong>Total paid out: </strong>  {$report.totalPayouts} | <strong>Current step: </strong> {$report.currentStepNumber}</p>
<p><strong>Hash:</strong>{$report.hash} | <strong>Last payout: </strong>{$latestLog.date_paid|crmDate:Full:0} | <strong>Last payment calculation: </strong>{$latestLog.date_calculated|crmDate:Full:0}</p>
<br />

{foreach from=$report.steps item=step key=stepIdx}
  {if $stepIdx+1 eq $report.currentStepNumber}
    {assign var="isCurrentStep" value=1}
  {else}
    {assign var="isCurrentStep" value=0}
  {/if}
<div class="panel panel-default report-step">
    <div class="panel-heading
        {if isset($step.dateCompleted)}bg-complete
        {elseif $isCurrentStep}bg-partial
        {else}bg-waiting{/if}">
        <div class="progress">
            <div class="progress-bar" role="progressbar" style="width:{$step.percentComplete}%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <h4><strong><span class="js-step-number">Step&nbsp;</span></strong><span class="js-step-description">&nbsp;- {$step.description} - {$step.type}{if $step.cap} ({ts}cap{/ts}: {$step.cap} ){/if}</span>&nbsp;
            <i class="crm-i
            {if $step.percentComplete eq '100'}fa-check-circle
            {elseif $step.percentComplete eq '0' or $step.totalPayouts eq null}fa-calendar
            {else}fa-rotate-right{/if}
            " aria-hidden="true"></i>
            <span class="pull-right">{if $step.totalPayouts != null}{$step.totalPayouts}  ({$step.percentComplete|number_format:2}%){/if}</span></h4>
    </div>
    <div class="panel-body padding0top">
        <table class="table table-hover table-condensed table-striped" id="js-payee-table1">
            <thead>
                <tr>
                    <th style="width: 40%">Name</th>
                    <th style="width: 15%">Last payout</th>
                    <th style="width: 15%">Paid to</th>
                    <th style="width: 15%">Owed ({$step.type})</th>
                    <th style="width: 15%; text-align: right;">Repaid</th>
                </tr>
            </thead>
            <tbody id="js-payee-table-body">
            {foreach from=$step.payees item=payee}
                <tr class="js-payeerow" id="js-payee0">
                    <td><strong class="js-payee-name"><a href="{crmURL p='civicrm/contact/view' q="reset=1&cid=`$payee.contact_id`"}">{$payee.paymentName}</a></strong>&nbsp;
                    <i class="crm-i
                    {if $payee.percentComplete eq '100'}fa-check-circle green
                    {elseif $payee.percentComplete eq '0' or $step.totalPayouts eq null}
                    {else}fa-rotate-right gold{/if}
                    " aria-hidden="true"></i></td>
                    <td class="js-payee-ac">{$payee.lastPayoutDate|crmDate:Full:0}</td>
                    <td class="js-payee-type">IBAN</td>
                    <td class="js-payee-amount">
                      {if $step.type eq 'fixed'}{$payee.paymentAmount}
                      {else}
                        {if $step.type eq 'percentage'}{$payee.paymentAmount} %
                        {else}{$payee.paymentAmount}{/if}
                      {/if}
                    </td>

                    <td class="{if $payee.percentComplete gte 100}bg-complete
                               {elseif $isCurrentStep}bg-partial
                               {else}bg-waiting{/if}">
                        {if isset($payee.percentComplete)}
                        <!-- @todo @nicol make this bar chart a line like the one for the step -->
                        <div class="progress high">
                          <div class="progress-bar" role="progressbar" style="width: {$payee.percentComplete}%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        {/if}
                        {$payee.totalPayouts} {if isset($payee.percentComplete)}({$payee.percentComplete}%){/if}
                        </div>
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
{/foreach}

<details open="">
<summary class="panel-heading">Payment history</summary>
  <table class="table table-hover table-condensed table-striped blue-border" id="js-payee-table1">
    <thead>
        <tr>
            <th style="width: 50%">Name</th>
            <th style="width: 20%">Paid to</th>
            <th style="width: 20%">Amount</th>
            <th style="width: 10%; text-align: right;">Status</th>
        </tr>
    </thead>
    <tbody id="js-payee-table-body">
    {assign var="lastDate" value=""}
    {foreach from=$report.logs item=log}
      {if $log.date_calculated != $lastDate}
        {assign var="lastDate" value=$log.date_calculated}
        <tr>
            <td colspan="5" class="bg-info"><strong>{$log.date_calculated|crmDate:Full:0}</strong></td>
        </tr>
      {/if}
      <tr class="js-payeerow" id="js-payee0">
          <td class="js-payee-name"><a href="{crmURL p='civicrm/contact/view' q="reset=1&cid=`$log.contact_id`"}">{$log.display_name}</a></strong></td>
          <td class="js-payee-type">IBAN</td>
          <td class="js-payee-amount"> {$log.amount}</td>
          <td class="js-status text-right">
              {if empty($log.date_paid)}Pending &nbsp;<i class="crm-i fa-rotate-right gold" aria-hidden="true"></i>{else}
              Paid &nbsp;<i class="crm-i fa-check-circle green" aria-hidden="true"></i>{/if}</td>
      </tr>
    {/foreach}
    </tbody>
  </table>

</details>
<!-- debugging -->
<pre style="display:none" >{$reportPretty}</pre>
</div>
