<?php
namespace Civi\Api4;

/**
 * CivisplitAgreementPaymentProcessor BridgeEntity.
 *
 * Provided by the CiviSplit extension.
 *
 * @searchable bridge
 * @package Civi\Api4
 */
class CivisplitAgreementPaymentProcessor extends Generic\DAOEntity {
  use Generic\Traits\EntityBridge;

}
