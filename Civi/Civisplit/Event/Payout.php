<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */
namespace Civi\Civisplit\Event;

/**
 * Class Payout
 *
 * This is a simple event that is triggered when we want to make payouts for a contact.
 * The "PayoutProcessor" eg. Uphold is expected to check:
 *   - Is the agreement using Uphold.
 *   - Does the contact have valid Payout details?
 *   - Does the contact have enough pending to make the Payout?
 */
class Payout extends \Symfony\Component\EventDispatcher\Event {

  /**
   * @var string
   */
  public $agreementName;

  /**
   * @var int
   */
  public $contactID;

  /**
   * FraudEvent constructor.
   *
   * @param string $agreementName
   * @param int $contactID
   */
  public function __construct(string $agreementName, int $contactID) {
    $this->agreementName = $agreementName;
    $this->contactID = $contactID;
  }

  /**
   * Use this to trigger an event from your code with a single line
   *
   * @param string $agreementName
   * @param int $contactID
   */
  public static function trigger(string $agreementName, int $contactID) {
    $event = new \Civi\Civisplit\Event\Payout($agreementName, $contactID);
    \Civi::dispatcher()->dispatch('civi.civisplit.payout', $event);
  }

}
