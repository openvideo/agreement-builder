<?php
/*
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC. All rights reserved.                        |
 |                                                                    |
 | This work is published under the GNU AGPLv3 license with some      |
 | permitted exceptions and without any warranty. For full license    |
 | and copyright information, see https://civicrm.org/licensing       |
 +--------------------------------------------------------------------+
 */
namespace Civi\Civisplit\Event;

/**
 * Class FundsAvailable
 *
 * This is triggered when we want the agreement to be processed.
 * The processor needs to calculate how much is actually available based on:
 *   - How much is pending for payout?
 *   - Do we need to retain any percentage to cover processing fees?
 */
class FundsAvailable extends \Symfony\Component\EventDispatcher\Event {

  /**
   * @var string
   */
  public $agreementHash;

  /**
   * @var string
   */
  public $amountAvailable;

  /**
   * FraudEvent constructor.
   *
   * @param string $agreementHash
   * @param string $amountAvailable
   */
  public function __construct(string $agreementHash, string $amountAvailable) {
    $this->agreementHash = $agreementHash;
    $this->amountAvailable = $amountAvailable;
  }

  /**
   * Use this to trigger an event from your code with a single line
   *
   * @param string $agreementHash
   * @param string $amountAvailable
   */
  public static function trigger(string $agreementHash, string $amountAvailable) {
    $event = new \Civi\Civisplit\Event\FundsAvailable($agreementHash, $amountAvailable);
    \Civi::dispatcher()->dispatch('civi.civisplit.funds.available', $event);
  }

}
