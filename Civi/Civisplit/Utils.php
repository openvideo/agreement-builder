<?php

namespace Civi\Civisplit;

use Psr\Log\LogLevel;

class Utils {

  /**
   * @param string $component
   * @param string $level
   */
  public static function log($component = 'civisplit', $level = LogLevel::DEBUG, $message) {
    switch ($level) {
      case LogLevel::INFO:
        // At INFO level we leave a permanent log (for validating the actions taken by civisplit).
        // @todo Write this to a database table
        \Civi::log()->info("{$component}: {$message}");
        break;

      case LogLevel::DEBUG:
      default:
        // @fixme We may want to switch this on/off via a setting
        \Civi::log()->debug("{$component}: {$message}");
        break;
    }
  }
}
