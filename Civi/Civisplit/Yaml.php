<?php
namespace Civi\Civisplit;

use \Brick\Money\Money;

/**
 * @class
 * YAML processing functions for CiviSplit.
 *
 * Note that there is a PECL extension to parse YAML, but we can't insist
 * people install PECL extensions.
 * https://www.php.net/manual/en/yaml.setup.php
 *
 * Note that this is a generic Yaml parser, though by no means does it support
 * every Yaml feature; only the minimum required to parse RSL 1.0.
 *
 */
class Yaml {

  public static $defaults = [
    'rsl'          => "",
    'name'         => "",
    'currency'     => "",
    'address'      => "",
    'contactName'  => "",
    'contactEmail' => "",
    'description'  => "",
    'repeatFor'    => "",
    'startDate'    => "",
    'endDate'      => "",
    'unit'         => "",
    'steps'        => []
  ];


  /**
   * Parse a value
   */
  public static function parseValue($v) {
    if (preg_match("/^'(.*)'$/", $v, $matches)) {
      // Unwrap a single quoted value, remove double single quotes
      return str_replace("''", "'", $matches[1]);
    }
    return $v;
  }

  /**
   * This is the main RSL parser method that returns an Array based PHP native
   * representation of the RSL.
   */
  public static function parse(string $yaml) {
    // Trim whitespace from start and finish and ensure we're using unix line endings.
    $yaml = preg_replace('/[\r\n]+/', "\n", trim($yaml, " \t\n\r"));

    $tokens = [ 'type' => 'Undefined' ];
    $container = &$tokens;
    $stack = [];
    $t = null;
    $indent = 0;
    $runaway = 300;

    $regexToken = function ($re) use (&$yaml) :?string {
      if (preg_match($re, $yaml, $matches)) {
        if (!empty($matches[1])) {
          // This token matched, remove it from the yaml.
          $yaml = mb_substr($yaml, mb_strlen($matches[0]));
          return $matches[1];
        }
      }
      return NULL;
    };

    while ($yaml && $runaway-- > 0) {
      $thisLineHasSameIndentAsLast = FALSE;

      // Look for new lines, we ignore these, but it's a good way to handle indentation changes.
      $t = $regexToken('/^(\n *)/');
      if ($t) {
        $thisLinesIndent = mb_strlen($t) - 1;
        while ($indent > $thisLinesIndent) {
          if ($container['type'] === 'Undefined') {
            $container['type'] = 'Value';
            $container['content'] = NULL;
          }
          unset($container);
          $container =  &$stack[count($stack)-1];
          array_pop($stack);
          $indent -= 2;
        }
        // If indent is the same, then we have to take care if we're already in a key.
        // because if the next thing we find is a '- ' list start, that means it's nested,
        // even though it does not have greater indent.
        $thisLineHasSameIndentAsLast = ($indent == $thisLinesIndent);

        // Store the new expected indent. (may be more than it was)
        $indent = $thisLinesIndent;
      }


      // If the current container is a multiline container, then we greedily add in everything up to \n
      if ($container['type'] === 'Multiline') {
        $t = $regexToken('/^(.*)(?:\n|$)/');
        if ($t) {
          $container['content'] .= (($container['content' ] === '') ? '' : "\n") . $t;
          // Add a new line back in so the indent is processed next.
          if ($yaml) {
            $yaml = "\n" . $yaml;
          }
          continue;
        }
      }

      // Look for keys.
      $t = $regexToken('/^([a-zA-Z][^ :\n]*) *: */');
      if ($t) {
        // Found a key token
        if ($container['type'] === 'Undefined'
            && $thisLineHasSameIndentAsLast) {
            // A key following a key with the same indent means the last key was empty.
            $container['type'] = 'Value';
            $container['content'] = '';
            unset($container);
            $container =  &$stack[count($stack)-1];
            array_pop($stack);
        }
        if ($container['type'] === 'Undefined') {
          // We need to define the container.
          $container['type'] = 'Object';
          $container['content'] = [];
        }
        if ($container['type'] === 'Object') {
          // Create an undefined item in this object at this key.
          $container['content'][$t] = [ 'type' => 'Undefined' ];
          // Push the current container onto the stack as we have a new one
          $stack[] = &$container;
          // The new container is the undefined container we just added under this key.
          $newContainer = & $container['content'][$t];
          unset($container);
          $container = &$newContainer;
          unset($newContainer);
          continue;
        }
        else {
          throw new \InvalidArgumentException("Can't handle a key token: " . json_encode($t) . " as not in an object");
        }
      }

      // Look for multiline start
      $t = $regexToken('/^(\|)\n/');
      if ($t) {
        // Found a multiline start token
        if ($container['type'] === 'Undefined') {
          // We need to define the container.
          $container['type'] = 'Multiline';
          $container['content'] = "";
          $yaml = "\n" . $yaml;
          continue;
        }
        else {
          throw new \InvalidArgumentException("Can't handle a Multiline token before " . mb_substr($yaml, 0, 30) . '...');
        }
      }

      // Look for list item start
      $t = $regexToken('/^(\-[ \n])/');
      if ($t) {
        if ($t === "-\n") {
          // Add the EOL back in.
          $yaml = "\n" . $yaml;
        }
        // Found a list item
        if ($container['type'] === 'Undefined') {
          // We need to define the container.
          $container['type'] = 'List';
          $container['content'] = [];
        }
        // if ($container['type'] !== 'List' && $mightNeedToGoUpOneMoreLevel && $stack[count($stack)]['type'] === 'List') {
        //   unset($container);
        //   $container = &array_pop($stack);
        // }
        if ($container['type'] === 'List') {
          // Create an undefined item and add to this array
          $listItem = [ 'type' => 'Undefined', 'content' => NULL ];
          $container['content'][] = &$listItem;
          // Push the current list container onto the stack
          $stack[] = &$container;
          // The new container is this listItem.
          unset($container);
          $container = &$listItem;
          unset($listItem);
          continue;
        }
        else {
          throw new \InvalidArgumentException("Can't handle a ListItem when container is " .
            json_encode($container, JSON_PRETTY_PRINT) . "\nyaml following:\n" . $yaml);
        }
      }

      // Look for inline list [ x,y, z ]
      // This is a list that is explicitly declared, as opposed to
      // lists that are declared implicitly by starting with a list item.
      $t = $regexToken('/^(\[) */');
      if ($t) {
        if ($container['type'] === 'Undefined') {
          // We need to define the container.
          $container['type'] = 'List';
          $container['content'] = [];
          // We can't push any list items; there might not be any.
          continue;
        }
        else {
          throw new \InvalidArgumentException("Can't handle a list when container is " . json_encode($container));
        }
      }
      // ... end of inline list.
      $t = $regexToken('/^(\]) */');
      if ($t) {
        if ($container['type'] === 'List') {
          // This is the end of the explicitly defined list.
          unset($container);
          $container =  &$stack[count($stack)-1];
          array_pop($stack);
          continue;
        }
        else {
          throw new \InvalidArgumentException("Can't handle a list end when container is " . json_encode($container));
        }
      }

      // Look for a single quoted value
      $t = $regexToken("/^'(([^']|'')*)'/");
      if ($t) {
        // remove '' for ' in single quoted strings
        $t = str_replace("''", "'", $t);
        if ($container['type'] === 'Undefined') {
          $container['type'] = 'Value';
          $container['content'] = $t;
          // Now we're done with that, pop this value, and the container is also done.
          unset($container);
          $container =  &$stack[count($stack)-1];
          array_pop($stack);
        }
        else if ($container['type'] === 'List') {
          $container['content'][] = ['type' => 'Value', 'content' => $t];
          // As we're in a list, it could be an inline list and this completed item could be followed by a comma.
          // Consume that now.
          $regexToken('/^( *, *)/');
        }
        else {
          throw new \InvalidArgumentException("Can't handle a ListItem when container is " .
            json_encode($container) . "\nyaml following:\n" . $yaml );
        }
        continue;
      }

      if ($container['type'] === 'List') {
        // Look for a value terminated by comma, or end of list ]
        $t = $regexToken('/^(.*?(?:, *|\]))/');
        if ($t) {
          preg_match('/(.*)(, *|\])$/', $t, $m);
          $container['content'][] = ['type' => 'Value', 'content' => $m[1]];
          // Unshift the last char back on the list if it was close-of-list
          if ($m[2] === ']') {
            $yaml = $m[2] . $yaml;
          }
          continue;
        }
      }

      // Look for values: at least something, up to a new line or end of source.
      $t = $regexToken('/^([^\n]+(\n|$))/');
      if ($t) {
        if ($container['type'] === 'Undefined') {
          preg_match('/^(.*)(\n)$/', $t, $m);
          if ($m) {
            $t = $m[1];
          }
          $container['type'] = 'Value';
          $container['content'] = $t;
          // Now we're done with that, pop this value, and the container is also done.
          unset($container);
          $container =  &$stack[count($stack)-1];
          array_pop($stack);
          if ($m) {
            $yaml = "\n" . $yaml;
          }
          continue;
        }
        else {
          throw new \InvalidArgumentException("ERROR got value " . json_encode($t) . " in container " . $container['type']);
          break;
        }
      }
    }

    unset($container);
    $struct = static::mapTokensToStruct($tokens) + static::$defaults;
    // print "Result:\n" . json_encode($struct, JSON_PRETTY_PRINT);

    if (($struct['rsl'] ?? '') === '1.0') {
      $struct = static::mapRSL1_0($struct);
    }
    // Later: may need to add further maps/filters for later RSL versions.

    // All numbers will be strings, but we want values to be in Brick\Money instances
    $struct = static::hydrateBrickMoney($struct);

    return $struct;
  }

  /**
   * Ensure the value is a string suitable for YAML.
   */
  public static function safeYamlValue($v) :string {
    if (gettype($v) === 'boolean') {
      $v = [FALSE => 'false', TRUE => 'true'][$v];
    }
    elseif ($v instanceof \Brick\Money\Money) {
      $v = (string) $v->getAmount();
    }
    else {
      $v = (string) ($v ?? '');
    }
    // Null, undefined: we want all these to go to a zero length string.
    // YAML is text based, convert non strings to strings
    // Check there's no new lines, we don't want these.
    if (preg_match('/[\r\n]/', $v)) {
      throw new \InvalidArgumentException("New lines are not allowed in this RSL YAML value.");
    }

    // We do not support special values yes/no/true/false - these should be quoted strings.
    if (preg_match('/["\':{}\[\]&*#?|<>=!%@\\,-]|(?:^(?:yes|no|true|false)$)/i', $v)) {
      // We will single quote this value.
      $v = "'" . str_replace("'", "''", $v) . "'";
    }
    return $v;
  }
  /**
   *
   */
  public static function safeYamlMultiline(string $k, $v, int $keyIndentation = 0) {
    $keyAndColon = str_repeat(' ', $keyIndentation) . "$k: ";
    if (is_string($v) && preg_match('/[\r\n]/', $v)) {
      // multiline needed
      return "$keyAndColon|\n" . preg_replace('/^/m', str_repeat(' ', $keyIndentation + 2), $v);
    }
    // Not needed
    return $keyAndColon . static::safeYamlValue($v);
  }

  /**
   * Convert the internal array structure representation of the agreement to RSL 1.0
   *
   */
  public static function toRSL(array $agreement) :string {
    // Required
    $lines = [
      'rsl: 1.0',
      'name: ' . static::safeYamlValue($agreement['name']),
      'currency: ' . static::safeYamlValue($agreement['currency']),
    ];

    // Optional
    foreach ([
      'decimals', 'address', 'contactName', 'contactEmail',
      'description', 'repeatFor', 'startDate', 'endDate', 'unit'
    ] as $optionalProp) {
      if (!empty($agreement[$optionalProp])) {
        $lines[] = static::safeYamlMultiline($optionalProp, $agreement[$optionalProp]);
      }
    };
    $lines[] = 'steps:';
    foreach ($agreement['steps'] ?? [] as $step) {
      $lines[] = '  -';
      foreach (['description', 'cap'] as $optionalProp) {
        if (!empty($step[$optionalProp])) {
          $lines[] = static::safeYamlMultiline($optionalProp, $step[$optionalProp], 4);
        }
      };

      $lines[] = static::safeYamlMultiline('type', $step['type'], 4);
      $lines[] = '    payees:';
      foreach ($step['payees'] ?? [] as $payee) {
        $lines[] = '      - [ '
          . static::safeYamlValue($payee['paymentName']) . ', '
          . static::safeYamlValue($payee['paymentAddress']) . ', '
          . static::safeYamlValue($payee['paymentType']) . ', '
          . static::safeYamlValue($payee['paymentAmount'])
          . ' ]';
      }
    }

    return implode("\n", $lines);
  }
  /**
   * For RSL1.0 we need to interpret the payee sequential arrays into their
   * named fields.
   */
  public static function mapRSL1_0(array $inputStruct) :array {
    $outputStruct = $inputStruct;
    foreach ($outputStruct['steps'] ?? [] as $stepIdx => $step) {
      foreach ($step['payees'] ?? [] as $payeeIdx => $payee) {
        $outputStruct['steps'][$stepIdx]['payees'][$payeeIdx] = [
          'paymentName'    => $payee[0],
          'paymentAddress' => trim($payee[1]),
          'paymentType'    => $payee[2],
          'paymentAmount'  => trim($payee[3]),
        ];
      }
    }
    return $outputStruct;
  }
  public static function mapTokensToStruct (array $container) {
    switch ($container['type']) {

    case 'Value':
      return $container['content'];

    case 'List':
    case 'Object':
      return array_map([static::class, 'mapTokensToStruct'], $container['content']);

    case 'Multiline':
      return $container['content'];

    case 'Undefined':
      return NULL;

    default:
      throw new \InvalidArgumentException("unrecognised container " . json_encode($container) . "\n" );
    }
  }

  /**
   * We want moneys to be Brick\Money objects, not strings.
   */
  public static function hydrateBrickMoney(array $inputStruct) :array {
    $outputStruct = $inputStruct;
    $currency = $inputStruct['currency'];
    $decimals = $inputStruct['decimals'] ?? 'period';

    $toBrick = function($stringNumber) use ($currency, $decimals) :Money {
      if ($decimals === 'comma') {
        $stringNumber = str_replace(',', '.', $stringNumber);
      }
      return Money::of($stringNumber, $currency);
    };


    if ($outputStruct['steps']) {
      foreach ($outputStruct['steps'] as $stepIdx => &$step) {
        if (isset($step['cap']) && $step['cap'] !== '') {
          $step['cap'] = $toBrick($step['cap']);
        }
        if ($step['type'] === 'fixed') {
          // paymentAmount values must be Money objects.
          foreach ($step['payees'] as $payeeIdx => &$payee) {
            if (isset($payee['paymentAmount'])) {
              $payee['paymentAmount'] = $toBrick($payee['paymentAmount']);
            }
          }
        }
      }
    }
    return $outputStruct;
  }
}

// cSpell:ignore PECL Civisplit
