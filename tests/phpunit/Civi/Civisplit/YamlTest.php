<?php
namespace Civi\Civisplit;

use CRM_Civisplit_ExtensionUtil as E;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;
use Civi\Civisplit\Yaml;
use Brick\Money\Money;

/**
 * FIXME - Add test description.
 *
 * Tips:
 *  - With HookInterface, you may implement CiviCRM hooks directly in the test class.
 *    Simply create corresponding functions (e.g. "hook_civicrm_post(...)" or similar).
 *  - With TransactionalInterface, any data changes made by setUp() or test****() functions will
 *    rollback automatically -- as long as you don't manipulate schema or truncate tables.
 *    If this test needs to manipulate schema or truncate tables, then either:
 *       a. Do all that using setupHeadless() and Civi\Test.
 *       b. Disable TransactionalInterface, and handle all setup/teardown yourself.
 *
 * @group headless
 */
class YamlTest extends \PHPUnit\Framework\TestCase implements HeadlessInterface, HookInterface, TransactionalInterface {
  public $defaultAgreement = [
      'rsl'          => "",
      'name'         => "",
      'currency'     => "",
      'address'      => "",
      'contactName'  => "",
      'contactEmail' => "",
      'description'  => "",
      'repeatFor'    => "",
      'startDate'    => "",
      'endDate'      => "",
      'unit'         => "",
      'steps'        => []
    ];

  public function setUpHeadless() {
    // Civi\Test has many helpers, like install(), uninstall(), sql(), and sqlFile().
    // See: https://docs.civicrm.org/dev/en/latest/testing/phpunit/#civitest
    return \Civi\Test::headless()
      ->installMe(__DIR__)
      ->apply();
  }

  public function setUp() {
    parent::setUp();
  }

  public function tearDown() {
    parent::tearDown();
  }

  /**
   * @dataProvider dataForTestParseYaml
   */
  public function testParseYaml($description, $yaml, $expected) {

    $expected += $this->defaultAgreement;
    $obj = Yaml::parse($yaml);

    // Convert any Brick\Money objects to strings.
    // This gives strings like 'USD 20.00' for 20 USD.
    // This way our test case can specify '123' for the number 123 and 'USD 123.00' for the currency.

    if (!empty($obj['steps'])) {
      foreach ($obj['steps'] as &$step) {
        if (isset($step['cap']) && $step['cap'] !== '') {
          $step['cap'] = (string) $step['cap'];
        }
        if (!empty($step['payees'])) {
          foreach ($step['payees'] as &$payee) {
            $payee['paymentAmount'] = (string) $payee['paymentAmount'];
          }
        }
      }
    }

    $this->assertEquals($expected, $obj, "FAIL: $description");
  }

  /**
   */
  public function dataForTestParseYaml() {
    $cases = [
      ['Description may be multiline',
      "description: |\n  Line one\n  Line two",
      ['description' => "Line one\nLine two"]
      ],

      ['Simple key-value',
      "name: Wilma\ncurrency: USD\ncontactName: 'Flintstone, Wilma'",
      ['name'=> "Wilma", 'currency'=> 'USD', 'contactName'=> 'Flintstone, Wilma']
      ],

      ['Simple key-value with nulls',
      "name: Wilma\ncurrency:USD\ncontactName:\naddress: \$ilp.blahblah",
      ['name' => 'Wilma', 'currency' => 'USD', 'contactName' => '', 'address' => '$ilp.blahblah']
      ],

      [
        'Steps test',
        "rsl: 1.0\ncurrency: USD\nname: Wilma\nsteps:\n-\n  description: Step uno\n  type: fixed\n  payees:\n    - [Wilma, \$wilma, ilp, 20]",
        [
          'rsl' => '1.0',
          'currency' => 'USD',
          'name'=> "Wilma",
          'steps'=> [
            [
              'description'=> "Step uno",
              'type'=> "fixed",
              'payees'=> [
                [ 'paymentName' => 'Wilma', 'paymentAddress' => '$wilma', 'paymentType' => "ilp", 'paymentAmount' => "USD 20.00" ]
              ]
            ],
          ]
        ]
      ],

      ['Generated RSL fixture 1',
      "rsl: 1.0\nname: 'Rich''s agreement'\ncurrency: USD\naddress: \$ipl.abc.def\ncontactName: Rich\ncontactEmail: Email\ndescription: |\n  Something cool\n  goes here\nrepeatFor: 1\nstartDate: '2021-11-02'\nunit: transactions\n"
      . "steps:\n"
      . "-\n  description: Pay off posters\n  type: fixed\n  cap: \n  payees:\n    - [Wendy, \$ilp.wendy, ilp, 100]\n"
      . "-\n  description: Pay off dark web spam posts\n  type: fixed\n  cap: \n  payees:\n    - [Anonymous Freddie, \$anon.1234, ilp, 300]\n"
      . "-\n  description: Creators\n  type: percentage\n  cap: \n"
      . "  payees:\n"
      . "    - [Betty, 012345, iban, 25]\n"
      . "    - [Bertie, 7890232, civicrm, 50]\n"
      . "    - [Barney, 1234, stripe, 25]",
      [
        'rsl' => "1.0",
        'name'=> "Rich's agreement",
        'currency'=> 'USD',
        'address'=> '$ipl.abc.def',
        'contactName'=> 'Rich',
        'contactEmail'=> 'Email', // xxx
        'description'=> "Something cool\ngoes here",
        'repeatFor'=> "1",
        'startDate'=> '2021-11-02',
        'unit'=> 'transactions',
        'steps'=> [
          [
            'description'=> "Pay off posters",
            'type'=> "fixed",
            'cap'=> '',
            'payees'=> [
              [ 'paymentName' => 'Wendy', 'paymentAddress' => '$ilp.wendy', 'paymentType' => "ilp", 'paymentAmount' => "USD 100.00" ],
            ]
          ],
          [
            'description'=> "Pay off dark web spam posts",
            'type'=> "fixed",
            'cap'=> '',
            'payees'=> [
              [ 'paymentName' => 'Anonymous Freddie', 'paymentAddress' => '$anon.1234', 'paymentType' =>  "ilp", 'paymentAmount' => "USD 300.00" ],
            ]
          ],
          [
            'description'=> "Creators",
            'type'=> "percentage",
            'cap'=> '',
            'payees'=> [
              [ 'paymentName' => 'Betty', 'paymentAddress' => '012345', 'paymentType' => "iban", 'paymentAmount' => "25" ],
              [ 'paymentName' => 'Bertie', 'paymentAddress' => '7890232', 'paymentType' => "civicrm", 'paymentAmount' => "50" ],
              [ 'paymentName' => 'Barney', 'paymentAddress' => '1234', 'paymentType' => "stripe", 'paymentAmount' => "25" ],
            ]
          ],
        ]
      ]
      ],
      [
        'Generated 2 - single quoted value in inline list',
        "rsl: 1.0\nname: boo\ncurrency: USD\ndecimals: period\nsteps:\n  -\n    description: uno\n    type: fixed\n    payees:\n      - ['Wilson, Billy', 29, civicrm, 123]",
        [
          'rsl' => "1.0",
          'name'=> "boo",
          'currency'=> 'USD',
          'decimals' => 'period',
          'steps'=> [
            [
              'description'=> "uno",
              'type'=> "fixed",
              'payees'=> [
                [ 'paymentName' => 'Wilson, Billy', 'paymentAddress' => '29', 'paymentType' => 'civicrm', 'paymentAmount' => 'USD 123.00']
              ]
            ],
          ]
        ],
      ],
      [
        'Decimals: period',
        "rsl: 1.0\ncurrency: USD\nname: Wilma\ndecimals: period\nsteps:\n-\n  type: fixed\n  payees:\n"
        . "    - [Wilma, \$wilma, ilp, 20.12]\n"
        . "    - [Wilma, \$wilma, ilp, 20]\n"
        . "    - [Wilma, \$wilma, ilp, 123456.78]\n",
        [
          'rsl' => '1.0',
          'currency' => 'USD',
          'decimals' => 'period',
          'name'=> "Wilma",
          'steps'=> [
            [
              'type'=> "fixed",
              'payees'=> [
                [ 'paymentName' => 'Wilma', 'paymentAddress' => '$wilma', 'paymentType' => "ilp", 'paymentAmount' => "USD 20.12" ],
                [ 'paymentName' => 'Wilma', 'paymentAddress' => '$wilma', 'paymentType' => "ilp", 'paymentAmount' => "USD 20.00" ],
                [ 'paymentName' => 'Wilma', 'paymentAddress' => '$wilma', 'paymentType' => "ilp", 'paymentAmount' => "USD 123456.78" ]
              ]
            ],
          ]
        ]
      ],
      [
        'Decimals: comma',
        "rsl: 1.0\ncurrency: EUR\nname: Wilma\ndecimals: comma\nsteps:\n-\n  type: fixed\n  payees:\n"
        . "    - [Wilma, \$wilma, ilp, '20,12']\n"
        . "    - [Wilma, \$wilma, ilp, 20]\n"
        . "    - [Wilma, \$wilma, ilp, '123456,78']\n",
        [
          'rsl' => '1.0',
          'currency' => 'EUR',
          'decimals' => 'comma',
          'name'=> "Wilma",
          'steps'=> [
            [
              'type'=> "fixed",
              'payees'=> [
                [ 'paymentName' => 'Wilma', 'paymentAddress' => '$wilma', 'paymentType' => "ilp", 'paymentAmount' => "EUR 20.12" ],
                [ 'paymentName' => 'Wilma', 'paymentAddress' => '$wilma', 'paymentType' => "ilp", 'paymentAmount' => "EUR 20.00" ],
                [ 'paymentName' => 'Wilma', 'paymentAddress' => '$wilma', 'paymentType' => "ilp", 'paymentAmount' => "EUR 123456.78" ]
              ]
            ],
          ]
        ]
      ],
      [
        'Decimals: none',
        "rsl: 1.0\ncurrency: JPY\nname: Wilma\ndecimals: comma\nsteps:\n-\n  type: fixed\n  payees:\n"
        . "    - [Wilma, \$wilma, ilp, 20]\n",
        [
          'rsl' => '1.0',
          'currency' => 'JPY',
          'decimals' => 'comma',
          'name'=> "Wilma",
          'steps'=> [
            [
              'type'=> "fixed",
              'payees'=> [
                [ 'paymentName' => 'Wilma', 'paymentAddress' => '$wilma', 'paymentType' => "ilp", 'paymentAmount' => "JPY 20" ],
              ]
            ],
          ]
        ]
      ],
    ];
    return array_map(function ($test) { $test[2] += $this->defaultAgreement; return $test; }, $cases);
  }
  /**
   * Tests the main YAML value encoding function.
   *
   * @dataProvider dataForTestSafeYamlMultiline
   */
  public function testSafeYamlMultiline(string $description, $raw, string $expected, $keyIndentation = 0) {
    $yaml = Yaml::safeYamlMultiline('k', $raw, $keyIndentation);
    $this->assertEquals(str_repeat(' ', $keyIndentation) . "k: $expected", $yaml, "FAIL: $description");
  }

  /**
   * Data provider for testToRSL
   */
  public function dataForTestSafeYamlMultiline() :array {
    return [
    ['Simple string', 'hello', 'hello'],
    ['Zero length string', '', ''],
    ['String with special chars ampersand used', 'People & Planet', "'People & Planet'"],
    ['String with apostrophe/single quote', "Lucy's Yak", "'Lucy''s Yak'"],
    ['String with double quotes', '"random" thoughts', "'\"random\" thoughts'"],
    ['Float should end up a string, with decimal point and no thousands separator', 1000.2345, "1000.2345"],
    ['Int should end up a string without thousands separator', 1000, "1000"],
    ['Null should be zero length string (for this project)', null, ""],
    // PHP does not have an 'undefined' value: ['Undefined should be zero length string (for this project)', undefined, ""],
    ['False should be stored as string', FALSE, "'false'"],
    ['True should be stored as string', TRUE, "'true'"],
    ['Yes should be treated as normal string', 'yes', "'yes'"],
    ['Norway problem: No should be treated as normal string', 'no', "'no'"],
    ['Norway problem - case should be ignored', 'No', "'No'"],

    ["Single line (multiline not required)", 'hello', "hello", 0],
    ["Single line with special chars (should be like safeYamlValue)", "Lucy's & Yak's", "'Lucy''s & Yak''s'", 0],
    ["Multi line simple", "One\nTwo", "|\n  One\n  Two", 0],
    ["Multi line with special chars (should be the same)", "One\n& Two", "|\n  One\n  & Two", 0],
    ["Multi line more indentation", "One\nTwo", "|\n    One\n    Two", 2],

    // Test brick money stuff.
    ['Brick Money object', Money::zero('USD'), '0.00'],
    ['Brick Money object', Money::of(20, 'GBP'), '20.00'],
    ];
  }

  /**
   * Complete test.
   */
  public function testToRSL() {

    $agreement = [
      'rsl' => '1.0',
      'name' => 'Test agreement',
      'currency' => 'USD',
      'decimals' => 'period',
      'description' => "Some\nagreement",
      'contactName' => 'May',
      'contactEmail' => 'may@example.org',
      'steps' => [
        [
          'description' => "Step Uno",
          'type' => 'fixed',
          'payees' => [
            [ 'paymentName' => 'Wilma', 'paymentAddress' => '$wilma', 'paymentType' => "ilp", 'paymentAmount' => Money::of(20, 'USD') ],
          ]
        ],
        [
          'description' => "Step Duo",
          'cap' => Money::of(100, 'USD'),
          'type' => 'ratio',
          'payees' => [
            [ 'paymentName' => 'Wilma', 'paymentAddress' => '$wilma', 'paymentType' => "ilp", 'paymentAmount' => "2" ],
            [ 'paymentName' => 'Betty', 'paymentAddress' => '$betty', 'paymentType' => "ilp", 'paymentAmount' => "1" ],
          ]
        ],
      ]
    ];

    $yaml = Yaml::toRSL($agreement);
    $this->assertEquals(<<<YAML
      rsl: 1.0
      name: Test agreement
      currency: USD
      decimals: period
      contactName: May
      contactEmail: 'may@example.org'
      description: |
        Some
        agreement
      steps:
        -
          description: Step Uno
          type: fixed
          payees:
            - [ Wilma, \$wilma, ilp, 20.00 ]
        -
          description: Step Duo
          cap: 100.00
          type: ratio
          payees:
            - [ Wilma, \$wilma, ilp, 2 ]
            - [ Betty, \$betty, ilp, 1 ]
      YAML, $yaml);
  }
}
