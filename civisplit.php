<?php

require_once 'civisplit.civix.php';
// phpcs:disable
use CRM_Civisplit_ExtensionUtil as E;
// phpcs:enable

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function civisplit_civicrm_config(&$config) {
  _civisplit_civix_civicrm_config($config);

  // Symfony hook priorities - see https://docs.civicrm.org/dev/en/latest/hooks/usage/symfony/#priorities
  \Civi::dispatcher()->addListener('hook_civicrm_navigationMenu', 'civisplit_symfony_civicrm_navigationMenu', 0);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function civisplit_civicrm_xmlMenu(&$files) {
  _civisplit_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function civisplit_civicrm_install() {
  _civisplit_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function civisplit_civicrm_postInstall() {
  _civisplit_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function civisplit_civicrm_uninstall() {
  _civisplit_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function civisplit_civicrm_enable() {
  _civisplit_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function civisplit_civicrm_disable() {
  _civisplit_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function civisplit_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _civisplit_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function civisplit_civicrm_managed(&$entities) {
  _civisplit_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function civisplit_civicrm_caseTypes(&$caseTypes) {
  _civisplit_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function civisplit_civicrm_angularModules(&$angularModules) {
  _civisplit_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function civisplit_civicrm_alterSettingsFolders(&$metaDataFolders = NULL) {
  _civisplit_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function civisplit_civicrm_entityTypes(&$entityTypes) {
  _civisplit_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_themes().
 */
function civisplit_civicrm_themes(&$themes) {
  _civisplit_civix_civicrm_themes($themes);
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
/**
 * Implementation of hook_civicrm_navigationMenu
 *
 * @param \Civi\Core\Event\GenericHookEvent $event
 * @param string $hookName
 */
function civisplit_symfony_civicrm_navigationMenu($event, $hookName) {
  _civisplit_civix_insert_navigation_menu($event->params, '', [
    'label' => E::ts('CiviSplit'),
    'name' => 'civisplit',
    'url' => '',
    'permission' => 'access CiviSplit',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _civisplit_civix_insert_navigation_menu($event->params, 'civisplit', [
    'label' => E::ts('Dashboard'),
    'name' => 'civisplit_agreementbuilder',
    'url' => 'civicrm/agreement-builder/index',
    'permission' => 'access CiviSplit',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _civisplit_civix_insert_navigation_menu($event->params, 'civisplit', [
    'label' => E::ts('Agreement Builder'),
    'name' => 'civisplit_agreementbuilder',
    'url' => 'civicrm/agreement-builder/create',
    'permission' => 'access CiviSplit',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _civisplit_civix_insert_navigation_menu($event->params, 'civisplit', [
    'label' => E::ts('Settings'),
    'name' => 'civisplit_settings',
    'url' => 'civicrm/admin/setting/civisplit',
    'permission' => 'administer CiviCRM',
    'operator' => 'OR',
    'separator' => 0,
  ]);
  _civisplit_civix_navigationMenu($event->params);
}
