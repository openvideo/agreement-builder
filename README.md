# civisplit

![Screenshot](/images/screenshot.png)

A drag and drop agreement builder using Cascade (https://github.com/openvideotech/cascade/) to create machine- and human-readable agreements using [Revenue Saring Language (RSL)](https://github.com/openvideotech/rsl).

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Installation

See: https://docs.civicrm.org/sysadmin/en/latest/customize/extensions/#installing-a-new-extension

## Getting Started

Go to /civicrm/agreement-builder

To load an existing agreement specify name as parameter. Eg.

    /civicrm/agreement-builder?agreementName=myagreement

## Known Issues

No permissions handling or tests.

## Agreement Viewer

Prototype only! Go to /civicrm/agreement
Eg. `/civicrm/agreement?hash=373f37d39d9678ed7ed88f886cc12817dbea712866e7bc6e0d6b7dc39e26b76e`

Parameters:

* hash - the hash from the `civicrm_civisplit_agreement` table. REQUIRED
* raw - if raw=1 then loads directly without using smarty template.
