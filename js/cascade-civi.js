// This is a non-compiled Javascript that enables you to use a CascadeApp.
// You must include this script *after* sourcing cascade.js
(($) => {
  let searchParams = new URLSearchParams(window.location.search);
  let agreementID = (searchParams.has('id') ? searchParams.get('id') : null);
  console.log("cascade-civi.js v0.2");

  document.addEventListener('DOMContentLoaded', () => {
    const localStorageId = 'MOVA-agreement';

    const cascade = CascadeApp.factory({
      target: document.getElementById('cascade-ui-placeholder'),
      props: {
        /**
         * The load() method must return a Promise that resolves to YAML.
         */
        async load() {
          console.log("cascade-civi load()");
          if (agreementID) {
            console.log("cascade-civi loading via API", agreementID);

            return CRM.api4('CivisplitAgreement', 'get',
              { where: [["id", "=", agreementID]]}, 0)
              .then(r => {
                console.log("API return", r);
                // Return required params.
                return {
                  readonly: r.status > 0, /* 0 = draft, 1 = generated, 2? = expired */
                  hash: r.hash,
                  yaml: r.agreement,
                };
              })
              .catch(failure => console.log(failure.error_message));
          }
          else {
            // Fallback to localStorage (not sure what use this is)
            console.log("cascade-civi loading from localStorage as no agreement ID found in query", localStorage.getItem(localStorageId));
            return Promise.resolve(localStorage.getItem(localStorageId));
          }
        },
        /**
         * The save() method stores the YAML and returns a Promise.
         */
        async save(rslYaml, name, finalise) {
          // Save to local storage for good measure.
          localStorage.setItem(localStorageId, rslYaml);

          let records = [{agreement: rslYaml, name, status_id: finalise ? 1 : 0}];
          if (agreementID) {
            records[0].id = agreementID;
          }
          console.log("cascade-civi saving via API", records[0]);
          return CRM.api4('CivisplitAgreement', 'save',
            { records }, 0)
            .then(r => {
              if (finalise) {
                // Redirect to agreement view page, using the hash we hopefully got back.
                window.location = CRM.url('civicrm/agreement-builder/view', {reset: 1, hash: r.hash});
              }
              else if (r.id && r.id != agreementID) {
                // Redirect to the URL for this agreement if it was new and is now saved.
                console.log("New agreement saved as ID", r.id, "...redirecting");
                window.location = CRM.url('civicrm/agreement-builder/create', {id: r.id});
              }
              else {
                console.log(`Agreement ${agreementID} saved.`);
                return r.agreement;
              }
            })
            .catch(failure => console.log(failure.error_message));
        },
        /**
         * ts() support
         * Note that we have to /call/ CRM.ts to get to the actual ts() function.
         */
        ts: CRM.ts(),

        /**
         *
         */
        onPayeeRowMounted(getPayeeRowComponent) {
          let payeeRow = getPayeeRowComponent();
          if (!payeeRow) {
            console.log("cascade-civi: cannot get payee row");
            return;
          }
          console.log("cascade-civi: got payee row", payeeRow);
          $(payeeRow.getPayeeAddressInput()).crmEntityRef({
            entity: 'Contact',
            create: false,
          })
          .on('change', (e) => {
            console.log("select 2 change:" , {e, pr: payeeRow});

            if (e.added && e.added.id) {
              console.log("updating");
              // Looks like they selected someone.
              payeeRow.assignNewRowData({
                name: e.added.label,
                paymentAddress: e.added.id,
                paymentType: 'civicrm'
              });
            }
          })
          ;
        },
        onPayeeRowDestroyed() {
          // Not used.
        },
        /**
         * This optionally maps certain class names to others. It enables the
         * output to the browser to support different CSS frameworks, as long
         * as they are happy to share the same HTML structure.
         *
         * See the .svelte files for the HTML classes to map from.
         */
        cssClassMap: {
      'agreement-name': 'col-sm-8',
      'agreement-description': 'col-sm-5',
      'agreement-group-1': '',
      'agreement-address': 'col-sm-3',
      'agreement-contact-name': 'col-sm-2',
      'agreement-email': 'col-sm-2',
      'form-inner-div': 'panel panel-default',
      'agreement-limit': 'panel panel-default',
      'limit-repeat': 'col-sm-3',
      'limit-unit': 'col-sm-3',
      'limit-start': 'col-sm-3',
      'limit-end': 'col-sm-3',
      'half' : 'col-sm-6',
      'halfish' : 'col-sm-6',
      'quarter' : 'col-sm-3',
      'tiny-column' : 'col-sm-2',
      'agreement-limit': 'limit-fieldset',
      'panel-heading': 'panel-heading',
      'panel-body': 'panel-body padding0top',
      'step-container': 'panel panel-default',
      'step-heading': 'panel-heading',
      'button-one': 'btn btn-primary crm-button',
      'button-two': 'btn btn-success crm-button',
      'button-alert': 'btn btn-warning crm-button',
      'icon-trash': 'crm-i fa-trash',
      'icon-edit': 'crm-i fa-edit',
      'icon-info': 'crm-i fa-info-circle',
      'icon-drag': 'crm-i fa-arrows-alt',
      'icon-tick': 'crm-i fa-check',
      'icon-add': 'crm-i fa-plus-circle',
    }
  }
});
});
})(CRM.$);
