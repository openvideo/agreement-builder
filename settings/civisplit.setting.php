<?php

use CRM_Civisplit_ExtensionUtil as E;

return [
  // financial_type
  'civisplit_financial_type' => [
    'name' => 'civisplit_financial_type',
    'type' => 'Integer',
    'html_type' => 'select',
    'default' => 0,
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => E::ts('Financial Type for CiviSplit transactions'),
    'description' => E::ts('Select the financial type for CiviSplit transactions'),
    'html_attributes' => [
      'placeholder' => E::ts('- select -'),
      'class' => 'crm-select2',
    ],
    'pseudoconstant' => ['callback' => 'CRM_Contribute_PseudoConstant::financialType'],
    'settings_pages' => [
      'civisplit' => [
        'weight' => 10,
      ]
    ],
  ]
];
